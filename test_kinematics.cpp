//#include <control_msgs/JointControllerState.h> // TODO: state message for all controllers?

#include <urdf/model.h>
//#include <hardware_interface/joint_command_interface.h>
//#include <controller_interface/controller.h>
#include <ros/node_handle.h>
#include <ros/ros.h>

#include <kdl/tree.hpp>
#include <kdl/kdl.hpp>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/frames.hpp>
#include <kdl/chaindynparam.hpp> //this to compute the gravity vector
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl_parser/kdl_parser.hpp>

#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <tf/transform_broadcaster.h>

#include <vector>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "lemon_delta_ik");
    
    ros::NodeHandle n;
    std::string robot_description;
    
    if (!ros::param::search(n.getNamespace(),"robot_description", robot_description))
    {
        ROS_ERROR_STREAM("KinematicChainControllerBase: No robot description (URDF) found on parameter server ("<<n.getNamespace()<<"/robot_description)");
        return false;
    }
    
    
    
    std::string xml_string;
    
    if (n.hasParam(robot_description))
        n.getParam(robot_description.c_str(), xml_string);
    else
    {
        ROS_ERROR("Parameter %s not set, shutting down node...", robot_description.c_str());
        n.shutdown();
        return false;
    }
    
    if (xml_string.size() == 0)
    {
        ROS_ERROR("Unable to load robot model from parameter %s",robot_description.c_str());
        n.shutdown();
        return false;
    }
    
    ROS_DEBUG("%s content\n%s", robot_description.c_str(), xml_string.c_str());
    
    
    urdf::Model model;
    if (!model.initString(xml_string))
    {
        ROS_ERROR("Failed to parse urdf file");
        n.shutdown();
        return false;
    }
    ROS_INFO("Successfully parsed urdf file");
    
    
    
    KDL::Tree kdl_tree_;
    
    
    if (!kdl_parser::treeFromUrdfModel(model, kdl_tree_))
    {
        ROS_ERROR("Failed to construct kdl tree");
        n.shutdown();
        return false;
    }
    
    
    std::vector<KDL::Chain> single_arms; single_arms.resize(3);
    std::vector<KDL::ChainFkSolverPos_recursive> fksolvers;
    std::vector<KDL::ChainIkSolverVel_pinv> ikvelsolvers;
    std::vector<KDL::ChainIkSolverPos_NR*> iksolvers;
    iksolvers.resize(3);
    std::vector<std::string> tip_names;
    tip_names.push_back("A_sixth_link");
    tip_names.push_back("B_sixth_link");
    tip_names.push_back("C_sixth_link");
    
    for (int i=0;i<3;i++)
    {
    // Populate the KDL chain a
        bool r = kdl_tree_.getChain("delta_box", tip_names[i], single_arms[i]);
        if(!r)
        {
            ROS_ERROR_STREAM("Failed to get KDL chain from tree: ");
            ROS_ERROR_STREAM("  "<<"delta_box"<<" --> "<<tip_names[i]);
            ROS_ERROR_STREAM("  Tree has "<<kdl_tree_.getNrOfJoints()<<" joints");
            ROS_ERROR_STREAM("  Tree has "<<kdl_tree_.getNrOfSegments()<<" segments");
            ROS_ERROR_STREAM("  The segments are:");
            
            KDL::SegmentMap segment_map = kdl_tree_.getSegments();
            KDL::SegmentMap::iterator it;
            
            for( it=segment_map.begin(); it != segment_map.end(); it++ )
                ROS_ERROR_STREAM( "    "<<(*it).first);
            return false;
        }
        fksolvers.emplace_back(single_arms[i]);
        ikvelsolvers.emplace_back(single_arms[i]);
        
    }
    for (int i=0;i<3;i++)
        iksolvers[i]= new KDL::ChainIkSolverPos_NR(single_arms[i],fksolvers[i],ikvelsolvers[i]);
    
    std::vector<KDL::JntArray> q_init,q_out;
    KDL::JntArray a;
    a.resize(single_arms[0].getNrOfJoints());
    for (int i=0;i<3;i++)
    {
        KDL::SetToZero(a);
        q_init.push_back(a);
        q_out.push_back(a);
        
    }
    
    KDL::Vector desired_cart(0,0,0.27);
    
    KDL::Vector Vector[3];
    Vector[0]=desired_cart;
    Vector[1]=desired_cart;
    Vector[2]=desired_cart;
    
    KDL::Rotation Rotation[3];
    Rotation[0] = KDL::Rotation::RotX(0.0)*KDL::Rotation::RotY(0.0)*KDL::Rotation::RotZ(-90*3.141592653589/180);
    Rotation[1] = KDL::Rotation::RotX(0.0)*KDL::Rotation::RotY(0.0)*KDL::Rotation::RotZ(+150*3.141592653589/180);
    Rotation[2] = KDL::Rotation::RotX(0.0)*KDL::Rotation::RotY(0.0)*KDL::Rotation::RotZ(+30*3.141592653589/180);
    //0.0223 offset from center to tip
    
    std::vector<KDL::Frame> desired_pos;
    int j=0;
    while(ros::ok())
    {   
        j++;
        for (int i=0;i<3;i++)
        {
            KDL::Frame temp(Rotation[i],Vector[i]);
            KDL::Frame temp1(KDL::Vector(-0.0223,0,0));
            KDL::Frame result= temp*temp1;
            desired_pos.push_back(result);
            static tf::TransformBroadcaster br;
            tf::Transform transform;
            transform.setOrigin( tf::Vector3(result.p.x(),result.p.y(),result.p.z()) );
            double x,y,z,w;
            result.M.GetQuaternion(x,y,z,w);
            tf::Quaternion q(x,y,z,w);        
            transform.setRotation(q);
            br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "delta_box", "M"+std::to_string(i)));
            
            ros::spinOnce();
            sleep(1);
        }
    
     for (int i=0;i<3;i++)
    {
         if (iksolvers[i]->CartToJnt(q_init[i], desired_pos[i], q_out[i]) < 0)
            ROS_INFO("solver for IK chain_a failed");
         for (int j=0;j<q_out[i].rows();j++)
            std::cout<<q_out[i](j)<<std::endl;
    }
    
    }
    
    
}