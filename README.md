Delta IK Controller
===================

This package is part of the Lemon Delta project suite. It contains a basic IK controller that allows to position the platform w.r.t. the base, using the the Delta driver.